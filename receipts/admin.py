from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt
# Register your models here.
@admin.register(ExpenseCategory)
class ExpenseCategoriesAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id"
    )

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
        "id"
    )

@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "id"
    )
