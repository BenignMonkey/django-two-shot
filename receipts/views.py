from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
# Create your views here.


@login_required
def receipts(request):
    list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "list": list
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    context = {
        'form': form,
        }
    return render(request, 'receipts/create.html', context)


@login_required
def category_list(request):
    items = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": items,
        }
    return render(request, 'receipts/categories.html', context)


@login_required
def account_list(request):
    items = Account.objects.filter(owner=request.user)
    context = {
        "accounts": items
        }
    return render(request, 'receipts/accounts.html', context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            form.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form
        }
    return render(request, 'receipts/expense_create.html', context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            form.save()
            return redirect('account_list')
    else:
        form = AccountForm()
    context = {
        "form": form
    }
    return render(request, "receipts/accounts_create.html", context)
